const http = require(`http`);

let port = 4000;

http.createServer((req, res) => {
    console.log("Server is successfully running");
    if(req.url === "/login"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.write("Welcome to the login page.");
        res.end();
    } else {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.write("I'm sorry the page you are looking for cannot be found.");
        res.end();
    }
}).listen(port);